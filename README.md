# Honest Pricing

Browser extension that rounds up prices to remove [psychological pricing](https://en.wikipedia.org/wiki/Psychological_pricing)

**Before**:

![before](doc/images/before.png)

**After**:

![after](doc/images/after.png)

