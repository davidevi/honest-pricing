


var price_pattern = /[£|$](\d*\.?[9]\d*)/ig

result = document.body.innerHTML;

do {
    match = price_pattern.exec(result)
    if (match) {
        as_float = parseFloat(match[1])
        rounded = Math.ceil(as_float)
        result = result.replace(match[1], rounded)
    }
}
while (match)

document.body.innerHTML = result;

